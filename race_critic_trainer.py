import math, Neural, random

pi = math.pi
c_Xlen = 7
c_Ylen = 11
c_Zlen = 1
LR = 0.2
dconst = 0.05

c_A = []
for y in range(0, c_Ylen):
      c_A.append([])
      for x in range(0, c_Xlen):
             c_A[y].append(random.uniform(-1.0,1.0))
c_B = []
for z in range(0, c_Zlen):
      c_B.append([])
      for y in range(0, c_Ylen):
             c_B[z].append(random.uniform(-1.0,1.0))


avgerror = 1
thresh = 0.9
n = 0
while avgerror > 0.0002:

    n+=1
    U_crit = 0
    sense_state = []
    for x in range(0, c_Xlen):
        sense_state.append(random.uniform(0.0,1.0))
    #randAngle = random.uniform(0.0,1.0)
    #randDiff = random.uniform(0.0,1.0)
    #randAngleOld = Neural.normalize((randAngle + randDiff),0.0,1.0,1)

    Rstate = []
    Rstate = list(sense_state)

    R_cY, R_crit = Neural.feedForward(Rstate, c_A, c_B)
    U_crit += (Rstate[0] - Rstate[6])
    U_crit += (Rstate[1] - Rstate[5])
    U_crit += (Rstate[2] - Rstate[4])
    U_crit += (Rstate[3])

    U_crit = Neural.sigmoid(U_crit)
    if abs(U_crit) > 1:
          print('Error!')

    error = 0.5*(R_crit-U_crit)**2
    avgerror = ((avgerror*50)+error)/51
    C_dz = (R_crit*(1-R_crit)+dconst)*(R_crit-U_crit)
    c_da, c_db = Neural.backpropogate(Rstate, c_A, R_cY, c_B, C_dz, 1)
    for j in range(0, len(c_da)):
        for i in range(0, len(c_da[j])):
            c_A[j][i] -= LR*c_da[j][i]
    for k in range(0, len(c_db)):
        for j  in range(0, len(c_db[k])):
            c_B[k][j] -= LR*c_db[k][j]
    if avgerror < thresh:
        print(avgerror,'--', LR, n)
        thresh = 0.9*thresh
            
print('saving')
File = open('critic_file', 'w')
for a in range(0, len(c_A)):
      for b in range(0, len(c_A[a])):
             File.write(str(c_A[a][b])+'\n')
for a in range(0, len(c_B)):
      for b in range(0, len(c_B[a])):
             File.write(str(c_B[a][b])+'\n')
File.close()

              
    
