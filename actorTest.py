#let the actor race

import pygame, math, Neural, race, sys, time, random
from pygame.locals import *


pygame.init()

pi = math.pi
L = 40
W = 20
a_Xlen = 8
a_Ylen = 4
a_Zlen = 1
#weights set up
a_A = []
filename = input('Actor File name: ')
actorFile = open(filename, 'r')
for y in range(0, a_Ylen):
      a_A.append([])
      for x in range(0, a_Xlen):
             a_A[y].append(float(actorFile.readline()))
a_B = []
for z in range(0, a_Zlen):
      a_B.append([])
      for y in range(0, a_Ylen):
             a_B[z].append(float(actorFile.readline()))
actorFile.close()

size = width, height = 1000, 600
screen = pygame.display.set_mode(size)
font = pygame.font.SysFont("Comic Sans MS", 10)
FailFont  = pygame.font.SysFont("Comic Sans MS", 60)
red = 255,0,0

filename = input('Track File name: ')
track, startLine = race.openTrack(filename)

start_theta = 0
start_x  = 0.5*(startLine[0]+startLine[1])
start_y  = 0.5*(startLine[2]+startLine[3])
trackPoly = race.SegToPoly(track, 2)

car = race.vehicle(start_x,start_y,start_theta, L, W)
track_angle = math.atan2((start_y-(height/2)),(start_x-(width/2)))
n_theta = Neural.normalize(track_angle,-pi,pi,1)

speed = 3
sensor_list = [-90,-60,-30,0,30,60,90]
senseProx = [0]*len(sensor_list)
R = 60 #sensor range

#Tells the number of joysticks/error detection
joystick_count = pygame.joystick.get_count()
print ("There is ", joystick_count, "joystick/s")
if joystick_count == 0:
       print ("Error, I did not find any joysticks")
else:
       print('Joystick found')
       my_joystick = pygame.joystick.Joystick(0)
       my_joystick.init()
while True:
       go = 0
       print('Press A to play/pause')
       while go == 0:
             for event in pygame.event.get():
                   if event.type == QUIT:
                         pygame.quit()
                         sys.exit()
              
             go = my_joystick.get_button(0)
             if my_joystick.get_button(1) == 1: break 

       car = race.vehicle(start_x,start_y,start_theta, L, W)
       n_theta_old = n_theta
       
       time.sleep(1)              
       car = race.drive(car,speed,0)              
       senseProx  = race.proximity(track, car, sensor_list, R)
       track_angle = math.atan2((start_y-(height/2)),(start_x-(width/2)))
       n_theta = Neural.normalize(track_angle,0,pi*2,1)
       Rstate = []
       for s in senseProx:
              Rstate.append(s)
       Rstate.append(n_theta)
       screen.fill((255,255,255))
       while True:
              for event in pygame.event.get():
                     if event.type == QUIT:
                            pygame.quit()
                            sys.exit()
              race.drawRace(screen, car, trackPoly, startLine, sensor_list, senseProx, 0, (0,255,0),1)

              #Run Actor network
              a_X = Rstate
              a_Y, a_steer = Neural.feedForward(a_X, a_A, a_B)

              #move using actor steering signal
              n_theta_old = n_theta
              steer = (a_steer*60)-30
              car = race.drive(car,speed,steer)
              track_angle = math.atan2((car[1]-(height/2)),(car[0]-(width/2)))
              n_theta = Neural.normalize(track_angle,-pi,pi,1)
              senseProx  = race.proximity(track, car, sensor_list, R)
              if senseProx == 'crash':
                     break
              Rstate = []
              for s in senseProx:
                     Rstate.append(s) 
              Rstate.append(n_theta)


##              #print data
##              data_out = [m_avgerror, h_dsteer, a_dsteer, SL_dsteer, n_theta]
##              for a in range(0, len(data_out)):
##                     posX = int(300)
##                     posY = int(300+a*10)
##                     screen.blit(font.render(str(data_out[a]), 2, red), (posX, posY))
              pygame.display.update()
              time.sleep(0.03)
              
