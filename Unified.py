#unified method
#from scratch

import pygame, math, Neural, race, sys, time, random, matplotlib.pyplot as plt
from pygame.locals import *

xw = 100
yw = 100
import os
os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (xw,yw)
pygame.init()

h_control = 1

pi = math.pi
L = 40
W = 20
joy_bias = 0.0
LR = 0.2
dconst = 0.05
a_Xlen = 8
a_Ylen = 4
a_Zlen = 1
a_avg = 0.25
h_avg = 0.25
a_avgError = []
a_soloError = []
h_avgError = []
i_Count = 0
j_Count = 0
#weights set up
a_A = []
for y in range(0,a_Ylen):
       a_A.append([])
       for x in range(0,a_Xlen):
              a_A[y].append(random.uniform(-1.0,1.0))
a_B = []
for z in range(0,a_Zlen):
       a_B.append([])
       for y in range(0,a_Ylen):
              a_B[z].append(random.uniform(-1.0,1.0))
a_avg = 0.25
h_avg = 0.25
a_soloavg = 0.25
a_avgError = []
a_soloError = []
h_avgError = []
i_Count = 0
j_Count = 0



m_Xlen = 9
m_Ylen = 11
m_Zlen = 11
m_Qlen = 7
m_A = []
m_B = []
m_C = []
m_avgerror = 1

c_Xlen = 7
c_Ylen = 11
c_Zlen = 1

#Menu----------------------
print('--------------------INPUT REQUIRED--------------------')       
K = float(input('Critic trust (K) = ')) 
LCF = input('Load Critic File?')
if LCF == 'y':
       c_A = []
       criticFile = open('critic_file', 'r')
       for y in range(0, c_Ylen):
              c_A.append([])
              for x in range(0, c_Xlen):
                     c_A[y].append(float(criticFile.readline()))
       c_B = []
       for z in range(0, c_Zlen):
              c_B.append([])
              for y in range(0, c_Ylen):
                     c_B[z].append(float(criticFile.readline()))
       criticFile.close()
else:
       c_A = []
       for y in range(0, c_Ylen):
              c_A.append([])
              for x in range(0, c_Xlen):
                     c_A[y].append(random.uniform(-1.0,1.0))
       c_B = []
       for z in range(0, c_Zlen):
              c_B.append([])
              for y in range(0, c_Ylen):
                     c_B[z].append(random.uniform(-1.0,1.0))
       
LMF = input('Load Model File?')
if LMF == 'y':
       m_A = []
       modelFile = open('model_file', 'r')
       for y in range(0, m_Ylen):
              m_A.append([])
              for x in range(0, m_Xlen):
                     m_A[y].append(float(modelFile.readline()))
       m_B = []
       for z in range(0, m_Zlen):
              m_B.append([])
              for y in range(0, m_Ylen):
                     m_B[z].append(float(modelFile.readline()))
       m_C = []
       for q in range(0, m_Qlen):
              m_C.append([])
              for z in range(0, m_Zlen):
                     #print(q,z, var, type(var), float(var))
                     m_C[q].append(float(modelFile.readline()))
       modelFile.close()
else:
       m_A = []
       for y in range(0, m_Ylen):
              m_A.append([])
              for x in range(0, m_Xlen):
                     m_A[y].append(random.uniform(-1.0,1.0))
       m_B = []
       for z in range(0, m_Zlen):
              m_B.append([])
              for y in range(0, m_Ylen):
                     m_B[z].append(random.uniform(-1.0,1.0))
       m_C = []
       for q in range(0, m_Qlen):
              m_C.append([])
              for z in range(0, m_Zlen):
                     
                     m_C[q].append(random.uniform(-1.0,1.0))
       
       
       


              


              
size = width, height = 1000, 600
screen = pygame.display.set_mode(size)
font = pygame.font.SysFont("Comic Sans MS", 10)
FailFont  = pygame.font.SysFont("Comic Sans MS", 60)
red = 255,0,0


track, startLine = race.openTrack('kidney')

start_theta = 0
start_x  = 0.5*(startLine[0]+startLine[1])
start_y  = 0.5*(startLine[2]+startLine[3])
trackPoly = race.SegToPoly(track, 2)

car = race.vehicle(start_x,start_y,start_theta, L, W)
track_angle = math.atan2((start_y-(height/2)),(start_x-(width/2)))
n_theta = Neural.normalize(track_angle,0,pi*2,1)

speed = 3
sensor_list = [-90,-60,-30,0,30,60,90]
senseProx = [0]*len(sensor_list)
R = 60 #sensor range

J_crit = 0

screen.fill((255,255,255))




#Tells the number of joysticks/error detection
joystick_count = pygame.joystick.get_count()
print ("There is ", joystick_count, "joystick/s")
if joystick_count == 0:
       print ("Error, I did not find any joysticks")
else:
       print('Joystick found')
       my_joystick = pygame.joystick.Joystick(0)
       my_joystick.init()
    
while True:
       print('pause game to save at any time')
       print('Press A to play/pause, B to quit, start to save')
       go = 0
       no = 0
       car = race.vehicle(start_x,start_y,start_theta, L, W)
       n_theta_old = n_theta
       
       while go == 0:
              for event in pygame.event.get():
                     if event.type == QUIT:
                            pygame.quit()
                            sys.exit()
              
              go = my_joystick.get_button(0)
              if my_joystick.get_button(1) == 1: break
              if  my_joystick.get_button(7) == 1:
                     time.sleep(0.3)
                     File = open('actor_file', 'w')
                     for a in range(0, len(a_A)):
                            for b in range(0, len(a_A[a])):
                                   File.write(str(a_A[a][b])+'\n')
                     for a in range(0, len(a_B)):
                            for b in range(0, len(a_B[a])):
                                   File.write(str(a_B[a][b])+'\n')
                     File.close()
                     File = open('model_file', 'w')
                     for a in range(0, len(m_A)):
                            for b in range(0, len(m_A[a])):
                                   File.write(str(m_A[a][b])+'\n')
                     for a in range(0, len(m_B)):
                            for b in range(0, len(m_B[a])):
                                   File.write(str(m_B[a][b])+'\n')
                     for a in range(0, len(m_C)):
                            for b in range(0, len(m_C[a])):
                                   File.write(str(m_C[a][b])+'\n')
                     File.close()
                     print('saved')
##                     File = open('critic_file', 'w')
##                     for a in range(0, len(c_A)):
##                            for b in range(0, len(c_A[a])):
##                                   File.write(str(c_A[a][b])+'\n')
##                     for a in range(0, len(c_B)):
##                            for b in range(0, len(c_B[a])):
##                                   File.write(str(c_B[a][b])+'\n')
##                     File.close()
       time.sleep(1)              
       car = race.drive(car,speed,0)              
       senseProx  = race.proximity(track, car, sensor_list, R)
       track_angle = math.atan2((start_y-(height/2)),(start_x-(width/2)))
       n_theta = Neural.normalize(track_angle,-pi,pi,1)
       Rstate = []
       for s in senseProx:
              Rstate.append(s)
       Rstate.append(n_theta)
       while True:
              
              for event in pygame.event.get():
                     if event.type == QUIT:
                            pygame.quit()
                            sys.exit()
              var = int((255)*(1-(J_crit)))
              var2 = int((255)*(1-4*((J_crit-0.5)**2)))
              varcol = 0,var2, 0
              if n_theta > math.radians(75) and n_theta < math.radians(105):
                     pygame.draw.rect(screen, green, [(400,300),(600,300),(600,400),(400,400)])
              
              race.drawRace(screen, car, trackPoly, startLine, sensor_list, senseProx, R, varcol)
              #pygame.draw.circle(screen, varcol, (int(car[0]),int(car[1])),5)
              #Pause Menu----------------------
              if my_joystick.get_button(0) == 1:
                     print('Paused -- press A to continue, B to demo, or start to save')

                     time.sleep(0.5)
                     go = 0
                     while go == 0:
                            for event in pygame.event.get():
                                   if event.type == QUIT:
                                          pygame.quit()
                                          sys.exit()
                            if my_joystick.get_button(0) == 1:
                                   #continue
                                   go = 1
                                   screen.fill((255,255,255))
                                   time.sleep(0.3)
                                   plt.close()
                            if my_joystick.get_button(2) == 1:
                                   #plot
                                   print(my_joystick.get_button(2))
                                   time.sleep(0.5)
                                   for event in pygame.event.get():
                                          if event.type == QUIT:
                                                 pygame.quit()
                                                 sys.exit()
                                   x = []
                                   for i in range(0,i_Count):
                                          x.append([i])
                                   print(len(x), len(a_avgError))
                                   a_E = plt.plot(x,a_soloError, label="Actor")
                                   h_E = plt.plot(x,h_avgError, label="Human")
                                   plt.legend(loc='upper right', shadow=True)
                                   plt.xlabel('Iterations')
                                   plt.ylabel('Error')
                                   plt.title('Error Over Time')
                                   plt.show()
                                   plt.close()
                                   time.sleep(1)
                                   
                            if my_joystick.get_button(1) == 1:
                                   #demo actor
                                   time.sleep(0.3)
                                   demo = 1
                                   while demo == 1:
                                          for event in pygame.event.get():
                                                 if event.type == QUIT:
                                                        pygame.quit()
                                                        sys.exit()
                                          if my_joystick.get_button(0) == 1:
                                                 demo = 0
                                          if my_joystick.get_button(2) == 1:
                                                 h_control = 0
                                          if my_joystick.get_button(1) == 1:
                                                 h_control = 1
                                          race.drawRace(screen, car, trackPoly, startLine, sensor_list, senseProx, R)

                                          #Run Actor network
                                          a_X = Rstate
                                          a_Y, a_steer = Neural.feedForward(a_X, a_A, a_B)
                                          a_mX = []
                                          for r in Rstate:
                                                 a_mX.append(r)
                                          a_mX.append(a_steer)
                                          a_mY, a_mZ, aState_next = Neural.feedForward(a_mX, m_A, m_B, m_C)
                                          a_cX=[]      
                                          for i in aState_next:
                                                 a_cX.append(i)
                                          a_cY, a_crit = Neural.feedForward(a_cX, c_A, c_B)
                                          
                                          a_error = (a_crit-0.5)**2
                                          a_soloavg = (a_soloavg*50+a_error)/51
                                          a_soloError.append(a_soloavg)
                                          h_avgError.append(0)
                                          i_Count += 1
                                          
                                          if h_control == 0:
                                                 a_cdz = ((a_crit)-0.5)*(a_crit*(1-a_crit)+dconst)
                                                 a_mdq = Neural.backpropogate(a_cX, c_A, a_cY, c_B, a_cdz)
                                                 a_dz = Neural.backpropogate2(a_mX, m_A, a_mY, m_B, a_mZ, m_C, a_mdq)
                                                 dsteer = a_dz[(len(a_dz)-1)] 
                                                 a_da, a_db = Neural.backpropogate(a_X, a_A, a_Y, a_B, dsteer, 1)
                                                 a_A = Neural.update(a_A, a_da, LR)
                                                 a_B = Neural.update(a_B, a_db, LR)
                                                 
                                          
                                          
                                          #move using actor steering signal
                                          steer = (a_steer*60)-30
                                          car = race.drive(car,speed,steer)
                                          Rstate = []
                                          for s in senseProx:
                                                 Rstate.append(s) 
                                          Rstate.append(n_theta)
                                          track_angle = math.atan2((car[1]-(height/2)),(car[0]-(width/2)))
                                          n_theta = Neural.normalize(track_angle,-pi,pi,1)
                                          senseProx  = race.proximity(track, car, sensor_list, R)
                                          if senseProx == 'crash':
                                                 break
                                          #print data
                                          data_out = [h_control]
                                          for a in range(0, len(data_out)):
                                                 posX = int(300)
                                                 posY = int(300+a*10)
                                                 screen.blit(font.render(str(data_out[a]), 2, red), (posX, posY))
                                          
                                          pygame.display.update()
                                          time.sleep(0.03)
                                   
                            if  my_joystick.get_button(7) == 1:
                                   #save 
                                   time.sleep(0.3)
                                   filename = "K"+str(K)+"_i"+str(i_Count)
                                   File = open(filename, 'w')
                                   for a in range(0, len(a_A)):
                                          for b in range(0, len(a_A[a])):
                                                 File.write(str(a_A[a][b])+'\n')
                                   for a in range(0, len(a_B)):
                                          for b in range(0, len(a_B[a])):
                                                 File.write(str(a_B[a][b])+'\n')
                                   File.close()
                                   File = open('model_file', 'w')
                                   for a in range(0, len(m_A)):
                                          for b in range(0, len(m_A[a])):
                                                 File.write(str(m_A[a][b])+'\n')
                                   for a in range(0, len(m_B)):
                                          for b in range(0, len(m_B[a])):
                                                 File.write(str(m_B[a][b])+'\n')
                                   for a in range(0, len(m_C)):
                                          for b in range(0, len(m_C[a])):
                                                 File.write(str(m_C[a][b])+'\n')
                                                 print('saved')
                                   File.close()
                                   File = open('critic_file', 'w')
                                   for a in range(0, len(c_A)):
                                          for b in range(0, len(c_A[a])):
                                                 File.write(str(c_A[a][b])+'\n')
                                   for a in range(0, len(c_B)):
                                          for b in range(0, len(c_B[a])):
                                                 File.write(str(c_B[a][b])+'\n')
                                   File.close()

              #Unified learning--------------------------
              
              #Run Actor network
              a_X = []
              for r in Rstate:
                     a_X.append(r)
              a_Y, a_steer = Neural.feedForward(a_X, a_A, a_B)
              #input human control
              j_steer = Neural.normalize(my_joystick.get_axis(0),-1,1) - joy_bias
              
              #model next states for both actions
              a_mX = []
              h_mX = []
              for r in Rstate:
                     a_mX.append(r)
                     h_mX.append(r)
              a_mX.append(a_steer)
              h_mX.append(j_steer)
              a_mY, a_mZ, aState_next = Neural.feedForward(a_mX, m_A, m_B, m_C)
              h_mY, h_mZ, hState_next = Neural.feedForward(h_mX, m_A, m_B, m_C)
              
              #if m_avgerror < 0.008:
              a_cX=[]      
              for i in aState_next:
                     a_cX.append(i)
              h_cX=[]      
              for i in hState_next:
                     h_cX.append(i)
              
              
              #Critique the future state
              a_cY, a_crit = Neural.feedForward(a_cX, c_A, c_B)
              h_cY, h_crit = Neural.feedForward(h_cX, c_A, c_B)


              #Update Actor
              
              #through critic
              h_cdz = ((h_crit)-0.5)*(h_crit*(1-h_crit)+dconst)
              a_cdz = ((a_crit)-0.5)*(a_crit*(1-a_crit)+dconst)
              h_mdq = Neural.backpropogate(h_cX, c_A, h_cY, c_B, h_cdz)
              a_mdq = Neural.backpropogate(a_cX, c_A, a_cY, c_B, a_cdz)

              #through model
              h_dz = Neural.backpropogate2(h_mX, m_A, h_mY, m_B, h_mZ, m_C, h_mdq)
              a_dz = Neural.backpropogate2(a_mX, m_A, a_mY, m_B, a_mZ, m_C, a_mdq)

              #isolate control variable
              h_dsteer = h_dz[(len(h_dz)-1)]
              
              a_dsteer = a_dz[(len(a_dz)-1)]    
              #Find Supervised Learning delta
              SL_dsteer = (a_steer*(1-a_steer)+dconst)*(a_steer - j_steer)
              #Combine learning methods
              dsteer = (K*(a_dsteer) + (1-K)*(SL_dsteer + K*h_dsteer))

##              elif K != 1:
##                     dsteer = (a_steer*(1-a_steer)+dconst)*(a_steer - j_steer)
##                     h_dsteer = 'learning'
##                     a_dsteer = 'learning'
##                     SL_dsteer = dsteer
##                     h_crit = 0
##              else:
##                     dsteer = [0]
##                     h_crit = 0
              #Update Actor weights
              a_da, a_db = Neural.backpropogate(a_X, a_A, a_Y, a_B, dsteer, 1)
              a_A = Neural.update(a_A, a_da, LR)
              a_B = Neural.update(a_B, a_db, LR)

              #Actor Error
              h_error = (h_crit-0.5)**2
              h_avg = (h_avg*50+h_error)/51
              h_avgError.append(h_avg)
              a_soloError.append(0)
              i_Count += 1
              

              #move using joystick input
              n_theta_old = n_theta
              steer = (j_steer*60)-30
              car = race.drive(car,speed,steer)
              track_angle = math.atan2((car[1]-(height/2)),(car[0]-(width/2)))
              n_theta = Neural.normalize(track_angle,-pi,pi,1)
              senseProx  = race.proximity(track, car, sensor_list, R)
              if senseProx == 'crash':
                     break

              

##              #Update Model
              Rstate = []
              m_hdq = []
              for s in senseProx:
                     Rstate.append(s)
                     m_hdq.append(0)
              
              m_error = 0
              for i in range(0, len(Rstate)):
                     m_error += 0.5*(hState_next[i] - Rstate[i])**2
                     m_hdq[i] = (hState_next[i]*(1-hState_next[i])+dconst)*(hState_next[i] - Rstate[i])
              m_hda, m_hdb, m_hdc = Neural.backpropogate2(h_mX, m_A, h_mY, m_B, h_mZ, m_C, m_hdq, 1)
              m_A = Neural.update(m_A, m_hda, LR)
              m_B = Neural.update(m_B, m_hdb, LR)
              m_C = Neural.update(m_C, m_hdc, LR)

              m_avgerror = ((50*m_avgerror)+m_error)/51 

              
##              #Update Critic
##              Rstate.append(n_theta_old)
              J_cY, J_crit = Neural.feedForward(Rstate, c_A, c_B)
##              U_crit = 0
##              for r in range(0, len(Rstate)-1):
##                     U_crit += 0.5*(Rstate[r]**2)
##              U_crit -= 0.5*((n_theta - n_theta_old)**2)
##              C_dz = (J_crit*(1-J_crit)+dconst)*(J_crit-U_crit)
##              c_da, c_db = Neural.backpropogate(Rstate, c_A, J_cY, c_B, C_dz, 1)
##              for j in range(0, len(c_da)):
##                     for i in range(0, len(c_da[j])):
##                            c_A[j][i] -= LR*Rstate[i]*c_da[j][i]
##              for k in range(0, len(c_db)):
##                     for j  in range(0, len(c_db[k])):
##                            c_B[k][j] -= LR*J_cY[j]*c_db[k][j]


              
              Rstate.append(n_theta)
              #print data
              data_out = [j_steer, a_steer, dsteer, n_theta, J_crit]
              for a in range(0, len(data_out)):
                     posX = int(300)
                     posY = int(300+a*10)
                     screen.blit(font.render(str(data_out[a]), 2, red), (posX, posY))
              pygame.display.update()
              time.sleep(0.03)
              
       

              
              
              
                            
              
              
              
              
              

              
