import pygame, math, race, sys, time, random, Neural
from pygame.locals import *
pi = math.pi
pygame.init()
L = 40
W = 20
c_Xlen = 7
c_Ylen = 11
c_Zlen = 1
size = width, height = 1000, 600
screen = pygame.display.set_mode(size)
font = pygame.font.SysFont("Comic Sans MS", 10)
FailFont  = pygame.font.SysFont("Comic Sans MS", 60)
red = 255,0,0


track, startLine = race.openTrack('stadium')

start_theta = (pi/2) + math.atan2((startLine[2]-startLine[0]),(startLine[3]-startLine[1]))
start_x  = 0.5*(startLine[0]+startLine[1])
start_y  = 0.5*(startLine[2]+startLine[3])
trackPoly = race.SegToPoly(track, 2)

car = race.vehicle(start_x,start_y,start_theta, L, W)
track_angle = math.atan2((start_y-(height/2)),(start_x-(width/2)))
n_theta = Neural.normalize(track_angle,0,pi*2,1)

speed = 3
sensor_list = [-90,-60,-30,0,30,60,90]
senseProx = [0]*len(sensor_list)
R = 60 #sensor range

U_crit = 0

screen.fill((255,255,255))

LCF = input('Load Critic File?')
if LCF == 'y':
       c_A = []
       criticFile = open('critic_file', 'r')
       for y in range(0, c_Ylen):
              c_A.append([])
              for x in range(0, c_Xlen):
                     c_A[y].append(float(criticFile.readline()))
       c_B = []
       for z in range(0, c_Zlen):
              c_B.append([])
              for y in range(0, c_Ylen):
                     c_B[z].append(float(criticFile.readline()))
       criticFile.close()


#Tells the number of joysticks/error detection
joystick_count = pygame.joystick.get_count()
print ("There is ", joystick_count, "joystick/s")
if joystick_count == 0:
       print ("Error, I did not find any joysticks")
else:
       print('Joystick found')
       my_joystick = pygame.joystick.Joystick(0)
       my_joystick.init()

       while True:
              
              for event in pygame.event.get():
                     if event.type == QUIT:
                            pygame.quit()
                            sys.exit()
              var = int((255)*(1-(abs(U_crit))*(1.0)))
              varcol = 0,var, 0
              
              race.drawRace(screen, car, trackPoly, startLine, sensor_list, senseProx, R, varcol)
              j_steer = Neural.normalize(my_joystick.get_axis(0),-1,1)
              #move using joystick input
              n_theta_old = n_theta
              steer = (j_steer*40)-20
              car = race.drive(car,speed,steer)
              track_angle = math.atan2((car[1]-(height/2)),(car[0]-(width/2)))
              n_theta = Neural.normalize(track_angle,-pi,pi,1)
              senseProx  = race.proximity(track, car, sensor_list, R)
              if senseProx == 'crash':
                     car = race.vehicle(start_x,start_y,start_theta, L, W)
                     senseProx  = race.proximity(track, car, sensor_list, R)
                     time.sleep(1)
              else:       
                     Rstate = [0]*len(senseProx)
                     for s in range(0, len(senseProx)):
                            Rstate[s] = (senseProx[s])
                     U_crit = 0
                     U_crit += (Rstate[0] - Rstate[6])
                     U_crit += (Rstate[1] - Rstate[5])
                     U_crit += (Rstate[2] - Rstate[4])
                     U_crit += (Rstate[3])
                        
                     U_crit = Neural.sigmoid(U_crit)-0.5

                     #print data
                     if LCF == 'y':
                            J_cY, J_crit = Neural.feedForward(Rstate, c_A, c_B)
                            data_out = [U_crit, J_crit-0.5]
                     else:
                            data_out = [U_crit]
                     for a in range(0, len(data_out)):
                            posX = int(300)
                            posY = int(300+a*10)
                            screen.blit(font.render(str(data_out[a]), 2, red), (posX, posY))
              pygame.display.update()
              time.sleep(0.03)
