#Network Visualizer

#Reads in network weights from a txt file. The weights are expected to be
#in the form of a 2 dimensional array. ie the first layer would be w_a[j][i]
#while the second layer would be w_b[k][j]. These arrays are filled line by line
#from the input file so that the first line of the file for a 3 layer network
#is a_w[0][0] with the second line being a_w[0][1]. There is no spacing or
#markers between weight layers so the node lengths must be input manually

import pygame, math
from pygame.locals import *

pygame.init()

size = width, height = 1000, 600
screen = pygame.display.set_mode(size)
font = pygame.font.SysFont("Comic Sans MS", 10)
red = 255,0,0
blue = 0,0,255
black = 0,0,0
white = 255,255,255
screen.fill(white)



file_name = input('Network File Name:  ')
file = open(file_name,'r')
layers = int(input('number of layers?'))
laylen = [0]*layers
for l in range(0,layers):
    laylen[l] = int(input(('node layer '+str(l+1)+' length?')))

W = []
for w in range(0, layers-1):
    W.append([])
    for j in range(0, laylen[(w+1)]):
        W[w].append([])
        ya = int(height/(layers+1))*(w+1)
        xb = int((width/(laylen[w+1]+1))*(j+1))
        yb = int((height/(layers+1))*(w+2))
        for i in range(0, laylen[(w)]):
            
            W[w][j].append(float(file.readline()))
            print(w,j,i, W[w][j][i])
            xa = int((width/(laylen[w]+1))*(i+1))
            Linewidth = 10*math.sqrt(abs(W[w][j][i]))
            if W[w][j][i] < 0:
                color = blue
            else:
                color = red
            
            pygame.draw.line(screen, color, (xb,yb), (xa,ya), int(abs(Linewidth)))



for event in pygame.event.get():
     if event.type == QUIT:
            pygame.quit()
            sys.exit()

            

for l in range(0, layers):
    y = int(height/(layers+1))*(l+1)
    for i in range(0, laylen[l]):
        x = int((width/(laylen[l]+1))*(i+1))
        pygame.draw.circle(screen, black, (x,y), 20)

pygame.display.update()
        
        
    

